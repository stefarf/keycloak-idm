module main

go 1.13

require (
	bitbucket.org/stefarf/iferr v0.0.0-20181220042854-d72473572c0c
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/kr/pretty v0.1.0
)
