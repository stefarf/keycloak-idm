// Testing jwt authentication using keycloack
package main

import (
	"bitbucket.org/stefarf/iferr"
	"crypto/rsa"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/kr/pretty"
	"io/ioutil"
)

const (
	rsaKid      = "H-M80AbRn_BQ9DBFmO7vsEdachL3u-7aXCxW0NcTHIw"
	tokenString = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJILU04MEFiUm5fQlE5REJGbU83dnNFZGFjaEwzdS03YVhDeFcwTmNUSEl3In0.eyJqdGkiOiJmYjk0NWI2YS0xMmYyLTQ0YjQtYjAxOC1jMDFkMjQ2NmJkZmQiLCJleHAiOjE1NzY1NzY0ODksIm5iZiI6MCwiaWF0IjoxNTc2NTc1NTg5LCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjUwMDAvYXV0aC9yZWFsbXMvbWFzdGVyIiwiYXVkIjoidi1kcml2ZSIsInN1YiI6IjU5ZTJkMjMxLWU3MmItNDdkYi1iNTYxLTZmY2NiNTdiNzhkNiIsInR5cCI6IklEIiwiYXpwIjoidi1kcml2ZSIsIm5vbmNlIjoiMmQ5MTI4MGEtMmIxZC00Y2YwLTk4N2EtMzAzMmYyYmI2ZjJhIiwiYXV0aF90aW1lIjoxNTc2NTczNTUyLCJzZXNzaW9uX3N0YXRlIjoiY2E0ODE5OWYtMmQ3Ni00NzYzLTlmZWItYjgxYTY0NDJjMjdjIiwiYWNyIjoiMCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZW1haWwiOiJhcmllZkB2b3N0cmEuY28uaWQifQ.M14EBmTo6url-jPNVGRE3Sa6hmNmP4ndlFJUpApTt6FQ7ygw3HJ80nqBy8ZbYJ6lglSyqc8bcXzpLDJWgd-ZaIFrh0ZcN-nyPfkdyanMMAW6e-_LrG7U0-tjaaJKmshxwjjaKNi4sxRj_iwEtKBDPgHe2GoJ6pGmLzFuHAqw7AzNajpQdHFV9KeZkxRYcXYaOC-0Z5wZ81iYwMtPCZ5i9MdtBpG8jDCQ8VdsS55vS7OV9CRztN-a07xsbD-Kl-kN7OwKDT7s8V8NKMmanBKHDSC36pdtiwgPnYIJ2Qbt6c4uq4rKm3JRzg98m2l6xhk5ekd77bE4yfFOKKd3iVf-PQ"
)

var (
	rsaPublicKey = readRSAPublicKey()
)

func readRSAPublicKey() *rsa.PublicKey {
	b, err := ioutil.ReadFile("pub.txt")
	iferr.Panic(err)
	bpub := []byte("-----BEGIN PUBLIC KEY-----\n" + string(b) + "\n-----END PUBLIC KEY-----")
	pub, err := jwt.ParseRSAPublicKeyFromPEM(bpub)
	iferr.Panic(err)
	return pub
}

func main() {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (i interface{}, e error) {
		kid := token.Header["kid"].(string)
		fmt.Println("kid:", kid)

		switch token.Method.(type) {
		case *jwt.SigningMethodRSA:
			if kid != rsaKid {
				return nil, errors.New("error kid")
			}
			return rsaPublicKey, nil
		default:
			return nil, errors.New("unexpected algorithm")
		}
	})
	iferr.Panic(err)
	pretty.Println(token)
}
