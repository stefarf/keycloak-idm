#!/usr/bin/env bash
docker exec keycloak \
    /opt/jboss/keycloak/bin/add-user-keycloak.sh \
    -u newuser \
    -p thepassword
docker restart keycloak
