#!/usr/bin/env bash

RUN_PORT=5000

docker network create keycloak-network 2>/dev/null

#docker pull postgres
docker stop postgres
docker rm postgres
docker run \
    -d --restart always \
    --name postgres \
    --net keycloak-network \
    -e POSTGRES_DB=keycloak \
    -e POSTGRES_USER=keycloak \
    -e POSTGRES_PASSWORD=password \
    postgres

#docker pull jboss/keycloak
docker stop keycloak
docker rm keycloak
docker run \
    -d --restart always \
    --name keycloak \
    --net keycloak-network \
    -e DB_ADDR=postgres \
    -e DB_USER=keycloak \
    -e DB_PASSWORD=password \
    -p $RUN_PORT:8080 \
    -e KEYCLOAK_USER=admin \
    -e KEYCLOAK_PASSWORD=admin \
    jboss/keycloak

docker system prune -f
echo "Please wait for a while and then open http://localhost:$RUN_PORT"
